#!/usr/bin/python

import sys
import subprocess
from multiprocessing import Pool
import os
import re
import json
import logging
import logging.handlers
import time
import random

# set logging
logging.basicConfig(filename='/var/log/stmfha.log', filemode='a', level=logging.DEBUG)

## Global variable
READONLY_ACTIONS = ('list-hg', 'list-lu', 'list-state',
                    'list-target', 'list-tg', 'list-view')

BACKUP_DEST = ''
## -----


# execute external programm and return status-code
def call(args):
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout = p.stdout.read()
    stderr = p.stderr.read()
    p.wait()
    p.stderr.flush()
    p.stdout.flush()
    logging.info('STMFHA. ' +  ' '.join(args) + '. RC: %s :: STDOUT: %s :: STDERR: %s ' % (p.returncode, stdout, stderr))
    return p.returncode, stdout, stderr


# setup
def do_setup():
    config = os.path.dirname(__file__) + '/../etc/stmfha.conf'
    with open(config, 'r') as f:
        settings = json.load(f)

    global BACKUP_DEST
    BACKUP_DEST = '/{pool}/.stmfha'.format(pool = settings['pool'])

    # check if backup directory exists
    # if not os.path.isdir(BACKUP_DEST):
    #     os.mkdir(BACKUP_DEST, 0755)

def do_stop():
    with open('%s/%s.lock' %(BACKUP_DEST, 'mutex'), 'w') as mutex:
        rc, output, stderr = call(['stmfadm', 'list-hg', '-v'])
        hgs = parse_hostgroup(output)
        logging.debug('STMFHA. ' + json.dumps(hgs))

        rc, output, stderr = call(['stmfadm', 'list-tg', '-v'])
        tgs =  parse_targetgroup(output)
        logging.debug('STMFHA. ' + json.dumps(tgs))

        rc, output, stderr = call(['stmfadm', 'list-lu', '-v'])
        lus = parse_stmflu(output)
        logging.debug('STMFHA. ' + json.dumps(lus))

        rc, output, stderr = call(['itadm','list-target', '-v'])
        targets = parse_target(output)
        logging.debug('STMFHA. ' + json.dumps(targets))

        rc, output, stderr = call(['itadm', 'list-tpg', '-v'])
        tpgs = parse_tpg(output)
        logging.debug('STMFHA. ' + json.dumps(tpgs))

        # delete lu in parallel
        args = [tuple(['stmfadm', 'delete-lu'] + [lu]) for lu in lus]
        p = Pool(10)
        map(lambda x: p.map(call, [x]), args)
        p.close()
        p.join()

        # delete all hg
        for hg in hgs:
            call(['stmfadm', 'delete-hg', hg])

        # delete all tg
        for tg in tgs:
            call(['stmfadm', 'delete-tg', tg])

        # delete all targets
        for target in targets:
            call(['itadm', 'delete-target', '-f', target])

        # delete all tpgs
        for tpg in tpgs:
            call(['itadm', 'delete-tpg', tpg])

    os.unlink('%s/%s.lock' % (BACKUP_DEST, 'mutex'))


def do_start():
    with open('%s/%s.lock' %(BACKUP_DEST, 'mutex'), 'w') as mutex:
        files = os.listdir(BACKUP_DEST)

        # check if stmf in non-persistent mode
        rc, output, stderr = call(['svccfg',  '-s',  'svc:/system/stmf',  'listprop', 'stmf_data/persist_method'])
        logging.info('STMFHA. Check if stmf in persistent mode. %s ' % output.strip())
        if output.split()[-1] == 'smf':
            logging.info('STMFHA. Switch to non-persistent mode.')
            call(['svccfg', '-s', 'svc:/system/stmf',  'setprop', 'stmf_data/persist_method=none'])
            call(['svcadm', 'refresh', 'svc:/system/stmf'])
            call(['svcadm', 'restart', 'svc:/system/stmf'])
        logging.info('STMFHA. Ensure that iscsi/target is running.')
        call(['svcadm', 'enable', '-r', 'iscsi/target'])

        # restore target port group
        for f_name in filter(lambda x: x[-3:] == 'tpg', files):
            with open('%s/%s' %(BACKUP_DEST, f_name), 'r') as fd:
                portal_list = json.load(fd)
                call(['itadm', 'create-tpg', f_name[:-4]] + portal_list)

        # restore targets
        for f_name in filter(lambda x: x[-6:] == 'target', files):
            with open('%s/%s' %(BACKUP_DEST, f_name), 'r') as fd:
                target = json.load(fd)
                args = ['itadm', 'create-target', '-n', f_name[:-7]]
                if target['tpg-tags'] != 'default':
                    args += ['-t', ",".join(target['tpg-tags'])]
                call(args)

        # restore host groups
        for f_name in filter(lambda x: x[-2:] == 'hg', files):
            with open('%s/%s' %(BACKUP_DEST, f_name), 'r') as fd:
                inq_list = json.load(fd)
                call(['stmfadm', 'create-hg', f_name[:-3]])
                for inq in inq_list:
                    call(['stmfadm', 'add-hg-member', '-g', f_name[:-3], inq])

        # restore target groups
        for f_name in filter(lambda x: x[-2:] == 'tg', files):
            with open('%s/%s' %(BACKUP_DEST, f_name), 'r') as fd:
                inq_list = json.load(fd)
                call(['stmfadm', 'create-tg', f_name[:-3]])
                for inq in inq_list:
                    call(['stmfadm', 'offline-target', inq])
                    call(['stmfadm', 'add-tg-member', '-g', f_name[:-3], inq])
                    call(['stmfadm', 'online-target', inq])

        # import lu
        for f_name in filter(lambda x: x[-2:] == 'lu', files):
            with open('%s/%s' %(BACKUP_DEST, f_name), 'r') as fd:
                lu = json.load(fd)
                lu_name = f_name[:-3]
                call(['stmfadm', 'import-lu', lu['DataFile']])


        # create views
        for f_name in filter(lambda x: x[-6:] == 'export', files):
            with open('%s/%s' %(BACKUP_DEST, f_name), 'r') as fd:
                view = json.load(fd)
                lu_name, entry_number = f_name[:-7].split('_')

                args = ['stmfadm', 'add-view']
                if view['TargetGroup'] != 'All':
                    args += ['-t', view['TargetGroup']]
                if view['Hostgroup'] != 'All':
                    args += ['-h', view['Hostgroup']]
                args += ['-n', str(view['LUN']), lu_name]

                call(args)

    os.unlink('%s/%s.lock' % (BACKUP_DEST, 'mutex'))

def do_backup():
    with open('%s/%s.lock' %(BACKUP_DEST, 'mutex'), 'w') as mutex:
        _, output, stderr = call(['stmfadm', 'list-hg', '-v'])
        hgs = parse_hostgroup(output)

        _, output, _ = call(['stmfadm', 'list-tg', '-v'])
        tgs =  parse_targetgroup(output)

        _, output, _ = call(['stmfadm', 'list-lu', '-v'])
        lus = parse_stmflu(output)

        _, output, _ = call(['itadm', 'list-tpg', '-v'])
        tpgs = parse_tpg(output)

        _, output, _ = call(['itadm', 'list-target', '-v'])
        targets = parse_target(output)

        exports = {}
        for lu in lus:
            rc, output, _ = call(['stmfadm', 'list-view', '-l', lu])
            if rc == 0:
                views = parse_view(output)
                for entry in views:
                    exports['%s_%s' % (lu, entry)] = views[entry]

        # save target port groups
        for tpg in tpgs:
            with open('%s/%s.tpg' % (BACKUP_DEST, tpg), 'w') as f:
                f.write(json.dumps(tpgs[tpg]))
                f.flush()

        # save targets
        for target in targets:
            with open('%s/%s.target' % (BACKUP_DEST, target), 'w') as f:
                f.write(json.dumps(targets[target]))
                f.flush()

        # save hg config
        for hg in hgs:
            with open('%s/%s.hg' % (BACKUP_DEST, hg), 'w') as f:
                f.write(json.dumps(hgs[hg]))
                f.flush()

        # save tg config
        for tg in tgs:
            with open('%s/%s.tg' % (BACKUP_DEST, tg), 'w') as f:
                f.write(json.dumps(tgs[tg]))
                f.flush()

        # save lu config
        for lu in lus:
            with open('%s/%s.lu' % (BACKUP_DEST, lu), 'w') as f:
                f.write(json.dumps(lus[lu]))
                f.flush()

        # save view config
        for export in exports:
            with open('%s/%s.export' % (BACKUP_DEST, export), 'w') as f:
                f.write(json.dumps(exports[export]))
                f.flush()

    os.unlink('%s/%s.lock' %(BACKUP_DEST, 'mutex'))


def parse_stmflu(output):
    numeric_fieds = ['BlockSize', 'Size', 'ViewEntryCount']
    logical_units = {}
    for lu_output in output.split('\n')[:-1]:
        key, value = lu_output.replace(' ', '').split(':')
        if key == 'LUName':
            lu_name = value
            logical_units[lu_name] = {}
            continue
        logical_units[lu_name][key]= int(value) if key in numeric_fieds else value
    return logical_units


def parse_view(output):
    numeric_fieds = ['ViewEntry', 'LUN']
    views = {}
    for view_output in output.split('\n')[:-1]:
        key, value = view_output.replace(' ', '').split(':')
        if key == 'ViewEntry':
            view_entry = value
            views[view_entry] = {}
            continue
        views[view_entry][key] = int(value) if key in numeric_fieds else value
    return views


def parse_hostgroup(output):
    hostgroups = {}
    for hg_output in output.split('\n')[:-1]:
        rows = hg_output.replace(' ', '').split(':')
        if rows[0] == 'HostGroup':
            hg_entry = rows[1]
            hostgroups[hg_entry] = []
            continue
        hostgroups[hg_entry].append(":".join(rows[1:]))
    return hostgroups


def parse_targetgroup(output):
    targetgroups = {}
    for hg_output in output.split('\n')[:-1]:
        rows = hg_output.replace(' ', '').split(':')
        if rows[0] == 'TargetGroup':
            tg_entry = rows[1]
            targetgroups[tg_entry] = []
            continue
        targetgroups[tg_entry].append(":".join(rows[1:]))
    return targetgroups


def parse_target(output):
    targets = {}
    curr_target = ''
    formated_output = re.sub(r' = [0-9]{1,3}', '', output)
    for line in formated_output.split('\n')[:-1]:
        rows = line.strip().split()

        if rows[0] == 'TARGET':
            continue
        if rows[0][:3] == 'iqn':
            curr_target = rows[0]
            targets[curr_target] = {}
            continue

        key = rows[0].replace(':', '')
        value = rows[1].split(',')
        targets[curr_target][key] = value

    return targets


def parse_tpg(output):
    tpgs = {}
    curr_tpg = ''

    for line in output.split('\n')[:-1]:
        rows = line.strip().split()
        if rows[0] == 'TARGET':
            continue

        if rows[0] != 'portals:':
            curr_tpg = rows[0]
            tpgs[curr_tpg] = {}
            continue

        if rows[0] == 'portals:':
            tpgs[curr_tpg] = rows[1].split(',')

    return tpgs


# lu functions
def delete_lu(args):
    """delete logical unit.
    remove it state and return rc"""
    args = ['stmfadm'] + args

    rc, stdout, stderr = call(args)
    if rc == 0:
        lu_name = args[-1]
        if os.path.isfile("%s/%s.lu"%(BACKUP_DEST, lu_name)):
            os.unlink("%s/%s.lu" % (BACKUP_DEST, lu_name))
        for f_name in os.listdir(BACKUP_DEST):
            if lu_name in f_name and f_name[-7:] == '.export':
                os.unlink("%s/%s" %(BACKUP_DEST, f_name))
    sys.exit(rc)


def create_lu(args):
    """create logical unit.
    save it state and return rc"""
    args = ['stmfadm'] + args

    # when we call stmfadm create-lu simultaneously, it returns the same UUID two times, which lead to failure.
    # the easiest way to fix it, just repeat export several times if it failed.
    repeat_max = 3
    repeat_count = 0
    rc = 0

    while repeat_count < repeat_max:
        rc, stdout, stderr = call(args)
        if rc == 0:
            repeat_count = 3
            continue
        time.sleep(random.randint(1,10))
        repeat_count += 1
        
    if rc == 0:
        lu_name = stdout.split()[-1]
        with open("%s/%s.lu" % (BACKUP_DEST, lu_name), 'w') as f:
            rc2, stdout2, stderr2 = call(['stmfadm', 'list-lu', '-v', lu_name])
            obj = parse_stmflu(stdout2)
            json.dump(obj[lu_name], f)
            f.flush()
        sys.stdout.write(stdout)
        sys.exit(rc|rc2)

    sys.exit(rc)

def add_view(args):
    """add view to lu and save it state"""
    args = ['stmfadm'] + args

    lu_name = args[-1]

    # when we call stmfadm add-view simultaneously, it returns the same LUN two times, which lead to failure.
    # the easiest way to fix it, just repeat export several times if it failed.
    repeat_max = 3
    repeat_count = 0
    rc = 0

    while repeat_count < repeat_max:
        rc, stdout, stderr = call(args)
        if rc == 0:
            repeat_count = 3
            continue
        time.sleep(random.randint(1, 10))
        repeat_count += 1

    if rc == 0:
        target_group = None
        host_group = None
        if '-t' in args:
            target_group = args[args.index('-t') + 1]
        else:
            target_group = 'All'
        if '-h' in args:
            host_group = args[args.index('-h') + 1]
        else:
            host_group = 'All'

        get_all_views_cmd = ['stmfadm', 'list-view', '-l', lu_name]
        rc2, stdout2, stderr2 = call(get_all_views_cmd)
        views = parse_view(stdout2)
        for view_entry in views:
            if views[view_entry]['Hostgroup'] == host_group and views[view_entry]['TargetGroup'] == target_group:
                with open("%s/%s_%s.export" %(BACKUP_DEST, lu_name, view_entry), 'w') as f:
                    json.dump(views[view_entry], f)
                    f.flush()
        sys.exit(rc2)

    sys.exit(rc)


def remove_view(args):
    """remove view and it's state"""
    args = ['stmfadm'] + args

    lu_name = None
    entry_num = None

    if '-a' in args:
        lu_name = args[-1]
    else:
        lu_name = args[-2]
        entry_num = args[-1]

    rc, stdout, stderr = call(args)

    if rc == 0:
        if '-a' in args:
            for f_name in os.listdir(BACKUP_DEST):
                if lu_name in f_name and f_name[-7:] == '.export':
                    os.unlink("%s/%s" % (BACKUP_DEST, f_name))
        elif entry_num is not None:
            os.unlink("%s/%s_%s.export" %(BACKUP_DEST, lu_name, entry_num))

    sys.exit(rc)


def create_hg(args):
    """create hostgroup"""
    args = ['stmfadm'] + args

    host_group = args[-1]

    rc, stdout, stderr = call(args)

    if rc == 0:
        with open("%s/%s.hg" %(BACKUP_DEST, host_group), 'w') as f:
            json.dump([], f)
            f.flush()

    sys.exit(rc)


def delete_hg(args):
    """delete hostgorup"""

    args = ['stmfadm'] + args

    host_group = args[-1]

    rc, stdout, stderr = call(args)

    if rc == 0:
         os.unlink("%s/%s.hg" % (BACKUP_DEST, host_group))

    sys.exit(rc)


def add_hg_member(args):
    """add member to hostgroup"""

    args = ['stmfadm'] + args

    host_group_index = None
    if '-g' in args:
        host_group_index = args.index('-g') + 1
    elif '--group-name' in args:
        host_group_index = args.index('--group-name') + 1

    host_group = args[host_group_index]

    rc, stdout, stderr = call(args)

    if rc == 0:
        get_hg_members = ['stmfadm', 'list-hg', '-v', host_group]
        rc2, stdout2, stderr2 = call(get_hg_members)
        hg_obj = parse_hostgroup(stdout2)
        with open("%s/%s.hg" %(BACKUP_DEST, host_group), 'w') as f:
            json.dump(hg_obj[host_group], f)
            f.flush()

    sys.exit(rc)


def remove_hg_member(args):
    """remove host group member"""

    # because actually internal logic for add member or delete member is the same
    add_hg_member(args)


def create_tg(args):
    "create target group"
    """create hostgroup"""
    args = ['stmfadm'] + args

    target_group = args[-1]

    rc, stdout, stderr = call(args)

    if rc == 0:
        with open("%s/%s.tg" %(BACKUP_DEST, target_group), 'w') as f:
            json.dump([], f)
            f.flush()

    sys.exit(rc)


def delete_tg(args):
    """delete target group"""
    args = ['stmfadm'] + args

    target_group = args[-1]

    rc, stdout, stderr = call(args)

    if rc == 0:
         os.unlink("%s/%s.tg" % (BACKUP_DEST, target_group))

    sys.exit(rc)


def add_tg_member(args):
    """add target group member"""
    args = ['stmfadm'] + args


    target_group_index = None
    if '-g' in args:
        target_group_index = args.index('-g') + 1
    elif '--group-name' in args:
        target_group_index = args.index('--group-name') + 1

    host_group = args[target_group_index]

    target_group = args[target_group_index]

    targets = args[target_group_index + 1:]

    for target in targets:
        offline_target_cmd = ['stmfadm', 'offline-target', target]
        call(offline_target_cmd)

    rc, stdout, stderr = call(args)

    for target in targets:
        offline_target_cmd = ['stmfadm', 'online-target', target]
        call(offline_target_cmd)

    if rc == 0:
        get_hg_members = ['stmfadm', 'list-tg', '-v', target_group]
        rc2, stdout2, stderr2 = call(get_hg_members)
        tg_obj = parse_targetgroup(stdout2)
        with open("%s/%s.tg" %(BACKUP_DEST, target_group), 'w') as f:
            json.dump(tg_obj[target_group], f)
            f.flush()

    sys.exit(rc)


def remove_tg_member(args):
    """remove target group member"""

    # actualy is has the same logic
    add_tg_member(args)


def create_tpg(args):
    """"create target port group"""
    args = ['itadm'] + args

    tpg_name = args[2]

    rc, stdout, stderr = call(args)

    rc2, stdout2, stderr2 = call(['itadm', 'list-tpg', '-v', tpg_name])
    tpgs = parse_tpg(stdout2)

    if rc == 0:
        with open("%s/%s.tpg" %(BACKUP_DEST, tpg_name), 'w') as f:
            json.dump(tpgs[tpg_name], f)
            f.flush()

    sys.exit(rc|rc2)


def delete_tpg(args):
    """delete target port group"""
    args = ['itadm'] + args

    tpg_name = args[2]

    rc, stdout, stderr = call(args)

    if rc == 0:
        os.unlink("%s/%s.tpg" %(BACKUP_DEST, tpg_name))

    sys.exit(rc)

def create_target(args):
    """create target"""

    args = ['itadm'] + args

    rc, stdout, stderr = call(args)

    if rc == 0:
        target_name = stdout.split()[1]
        list_target_cmd = ['itadm', 'list-target', '-v', target_name]
        rc2, stdout2, stderr2 = call(list_target_cmd)
        targets = parse_target(stdout2)
        with open("%s/%s.target" %(BACKUP_DEST, target_name), 'w') as f:
            json.dump(targets[target_name], f)
            f.flush()
        sys.exit(rc2)

    sys.exit(rc)


def delete_target(args):
    """delete target"""
    args = ['itadm'] + args

    rc, stdout, stderr = call(args)

    if rc == 0:
        target_name = args[-1]
        os.unlink("%s/%s.target" %(BACKUP_DEST, target_name))

    sys.exit(rc)


if __name__ == '__main__':

    # check if we have enought arguments to execute stmfadm
    if len(sys.argv) == 1:
        rc, stdout, stderr = call(['stmfadm'])
        sys.stdout.write(stdout)
        sys.stderr.write(stderr)
        sys.exit(rc)

    # check if we call readonly actions
    if sys.argv[1] in READONLY_ACTIONS:
        COMMAND = ['stmfadm'] + sys.argv[1:]
        rc, stdout, stderr = call(COMMAND)
        sys.stdout.write(stdout)
        sys.stderr.write(stderr)
        sys.exit(rc)

    # actions with which result we need preserve after reboot
    do_setup()

    action = sys.argv[1]

    if action == 'backup':
        do_backup()
    elif action == 'stop':
        do_stop()
    elif action == 'start':
        do_start()
    elif action == 'delete-lu':
        delete_lu(sys.argv[1:])
    elif action == 'create-lu':
        create_lu(sys.argv[1:])
    elif action == 'add-view':
        add_view(sys.argv[1:])
    elif action == 'remove-view':
        remove_view(sys.argv[1:])
    elif action == 'create-hg':
        create_hg(sys.argv[1:])
    elif action == 'delete-hg':
        delete_hg(sys.argv[1:])
    elif action == 'add-hg-member':
        add_hg_member(sys.argv[1:])
    elif action == 'remove-hg-member':
        remove_hg_member(sys.argv[1:])
    elif action == 'create-tg':
        create_tg(sys.argv[1:])
    elif action == 'delete-tg':
        delete_tg(sys.argv[1:])
    elif action == 'add-tg-member':
        add_tg_member(sys.argv[1:])
    elif action == 'remove-tg-member':
        remove_tg_member(sys.argv[1:])
    elif action == 'create-tpg':
        create_tpg(sys.argv[1:])
    elif action == 'delete-tpg':
        delete_tpg(sys.argv[1:])
    elif action == 'create-target':
        create_target(sys.argv[1:])
    elif action == 'delete-target':
        delete_target(sys.argv[1:])
    else:
        logging.info('STMFHA. Action is not defined: ' + str(sys.argv[1:]))