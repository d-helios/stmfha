# STMFHA

## FAQ
### Add binary path
```bash
export PATH=/opt/stmfha/bin:$PATH
```

### Start stmfha 
While starting stmfha perform following steps:

* load all configuration from __pool_name__/.stmfha
* try to restore configuration in following order:
    * target port group (__itadm create-tpg__)
    * targets (__itadm create_target__)
    * target_group (__stmfadm create-tg && stmfadm add-tg-member__)
    * host_group (__stmfadm create-hg && stmfadm add-hg-member__)
    * import lu (__stmfadm import-lu__)
    * add-view (__stmfadm add-view__)
    
__SMTFHA debug output is /var/log/stmfha.log__

Sun Cluster user script __/opt/stmfha/bin/stmfha_resource_start__ while perform enabling resource and __/opt/stmfha/bin/stmfha_resource_stop__ while disabling resource.


If you wish to perform manualy backup of effective configuration, run  __/opt/stmfha/bin/stmfha_resource_backup__

    
```bash
root@znstor6-n1:~# /opt/stmfha/bin/stmfha_resource_start

root@znstor6-n1:~# tail -40 /var/log/stmfha.log
INFO:root:STMFHA. svccfg -s svc:/system/stmf listprop stmf_data/persist_method. RC: 0 :: STDOUT: stmf_data/persist_method astring     none
 :: STDERR:
INFO:root:STMFHA. Check if stmf in persistent mode. stmf_data/persist_method astring     none
INFO:root:STMFHA. Ensure that iscsi/target is running.
INFO:root:STMFHA. svcadm enable -r iscsi/target. RC: 0 :: STDOUT:  :: STDERR:
```
### Create target port group
```bash
root@znstor6-n1:~# stmfha create-tpg cinder-dev-tpg 172.30.204.5
 
root@znstor6-n1:~# tail -5 /var/log/stmfha.log
INFO:root:STMFHA. itadm create-tpg cinder-dev-tpg 172.30.204.5. RC: 0 :: STDOUT:  :: STDERR:
INFO:root:STMFHA. itadm list-tpg -v cinder-dev-tpg. RC: 0 :: STDOUT: TARGET PORTAL GROUP           PORTAL COUNT
cinder-dev-tpg                1
    portals:    172.30.204.5:3260
 :: STDERR:
 
root@znstor6-n1:~# itadm list-tpg -v
TARGET PORTAL GROUP           PORTAL COUNT
cinder-dev-tpg                1
    portals:    172.30.204.5:3260
   
root@znstor6-n1:~# ls -l /dpool5/.stmfha
total 19
-rw-r--r--   1 root     root          21 Feb 20 00:36 cinder-dev-tpg.tpg
root@znstor6-n1:~# cat /dpool5/.stmfha/cinder-dev-tpg.tpg
["172.30.204.5:3260"]
```

### Create target
```bash
root@znstor6-n1:~# stmfha create-target -t cinder-dev-tpg
root@znstor6-n1:~# itadm list-target -v
TARGET NAME                                                  STATE    SESSIONS
iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371  online   0
        alias:                  -
        auth:                   none (defaults)
        targetchapuser:         -
        targetchapsecret:       unset
        tpg-tags:               cinder-dev-tpg = 2
 
root@znstor6-n1:~# tail -10 /var/log/stmfha.log        
INFO:root:STMFHA. itadm create-target -t cinder-dev-tpg. RC: 0 :: STDOUT: Target iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371 successfully created
 :: STDERR:
INFO:root:STMFHA. itadm list-target -v iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371. RC: 0 :: STDOUT: TARGET NAME                                                  STATE    SESSIONS
iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371  online   0
        alias:                  -
        auth:                   none (defaults)
        targetchapuser:         -
        targetchapsecret:       unset
        tpg-tags:               cinder-dev-tpg = 2
 :: STDERR:
 
 root@znstor6-n1:~# ls -l /dpool5/.stmfha
total 38
-rw-r--r--   1 root     root          21 Feb 20 00:36 cinder-dev-tpg.tpg
-rw-r--r--   1 root     root         122 Feb 20 00:49 iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371.target
root@znstor6-n1:~# cat /dpool5/.stmfha/iqn.1986-03.com.sun\:02\:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371.target
{"alias": ["-"], "tpg-tags": ["cinder-dev-tpg"], "targetchapuser": ["-"], "auth": ["none"], "targetchapsecret": ["unset"]}
```

### Create initiator host group
```bash
root@znstor6-n1:~# stmfha create-hg test-hg
root@znstor6-n1:~# stmfadm list-hg -v
Host Group: test-hg
 
root@znstor6-n1:~# ls -l  /dpool5/.stmfha/
total 57
-rw-r--r--   1 root     root          21 Feb 20 01:28 cinder-dev-tpg.tpg
-rw-r--r--   1 root     root         122 Feb 20 01:28 iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371.target
-rw-r--r--   1 root     root           2 Feb 20 01:42 test-hg.hg
 
root@znstor6-n1:~# cat /dpool5/.stmfha/test-hg.hg
[]
```

### Add member to host group
```bash
root@znstor6-n1:~# stmfha add-hg-member -g test-hg iqn.1986-03.com.sun:01:e00000000000.5a607ec4
 
root@znstor6-n1:~# stmfadm list-hg -v
Host Group: test-hg
        Member: iqn.1986-03.com.sun:01:e00000000000.5a607ec4
 
root@znstor6-n1:~# cat /dpool5/.stmfha/test-hg.hg
["iqn.1986-03.com.sun:01:e00000000000.5a607ec4"]
```

### Remove member from host group
```bash
["iqn.1986-03.com.sun:01:e00000000000.5a607ec4"]root@znstor6-n1:~# stmfha remove-hg-member -g test-hg iqn.1986-03.com.sun:01:e00000000000.5a607ec4
 
root@znstor6-n1:~# cat /dpool5/.stmfha/test-hg.hg
[]
```

### Delete host group
```bash
root@znstor6-n1:~# stmfha delete-hg test-hg
 
root@znstor6-n1:~# ls -l /dpool5/.stmfha
total 38
-rw-r--r--   1 root     root          21 Feb 20 01:28 cinder-dev-tpg.tpg
-rw-r--r--   1 root     root         122 Feb 20 01:28 iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371.target
```

### Create target group
```bash
root@znstor6-n1:~# stmfha create-tg test-tg
root@znstor6-n1:~# stmfadm list-tg -v
Target Group: test-tg
root@znstor6-n1:~# cat /dpool5/.stmfha/test-tg.tg
[]
```

### Add target to target group
All targets  added in a following way
* __offline__ target
* add target to target group
* __online__ target

```bash
root@znstor6-n1:~# stmfha add-tg-member -g test-tg iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371
root@znstor6-n1:~# stmfadm list-tg -v
Target Group: test-tg
        Member: iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371
        
root@znstor6-n1:~# cat /dpool5/.stmfha/test-tg.tg
["iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371"]
```

### Remove target from target group
All targets removed in a following way
* __offline__ target
* remove target to target group
* __online__ target

```bash
root@znstor6-n1:~# stmfha remove-tg-member -g test-tg iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371
root@znstor6-n1:~# stmfadm list-tg -v
Target Group: test-tg
 
root@znstor6-n1:~# cat /dpool5/.stmfha/test-tg.tg
[]
```

### Delete target group
You can't delete target group until it's not empty
```bash
root@znstor6-n1:~# stmfadm create-tg test-tg
root@znstor6-n1:~# stmfha delete-tg test-tg
root@znstor6-n1:~# stmfadm list-tg -v
root@znstor6-n1:~# ls -l /dpool5/.stmfha/
total 38
-rw-r--r--   1 root     root          21 Feb 20 01:28 cinder-dev-tpg.tpg
-rw-r--r--   1 root     root         122 Feb 20 01:28 iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371.target
root@znstor6-n1:~#
```

### Create LU
```bash
root@znstor6-n1:~# for i in {1..10}; do stmfha create-lu /dev/zvol/rdsk/dpool5/vols/vol-${i}; done
Logical unit created: 600144F007D38C0000005A8B583E0001

Logical unit created: 600144F007D38C0000005A8B58410002

Logical unit created: 600144F007D38C0000005A8B58410003

Logical unit created: 600144F007D38C0000005A8B58420004

Logical unit created: 600144F007D38C0000005A8B58420005

Logical unit created: 600144F007D38C0000005A8B58420006

Logical unit created: 600144F007D38C0000005A8B58420007

Logical unit created: 600144F007D38C0000005A8B58430008

Logical unit created: 600144F007D38C0000005A8B58430009

Logical unit created: 600144F007D38C0000005A8B5843000A

root@znstor6-n1:~#

root@znstor6-n1:~# stmfadm list-lu
LU Name: 600144F007D38C0000005A8B583E0001
LU Name: 600144F007D38C0000005A8B58410002
LU Name: 600144F007D38C0000005A8B58410003
LU Name: 600144F007D38C0000005A8B58420004
LU Name: 600144F007D38C0000005A8B58420005
LU Name: 600144F007D38C0000005A8B58420006
LU Name: 600144F007D38C0000005A8B58420007
LU Name: 600144F007D38C0000005A8B58430008
LU Name: 600144F007D38C0000005A8B58430009
LU Name: 600144F007D38C0000005A8B5843000A
root@znstor6-n1:~# ls -l /dpool5/.stmfha
total 228
-rw-r--r--   1 root     root         478 Feb 20 02:05 600144F007D38C0000005A8B583E0001.lu
-rw-r--r--   1 root     root         478 Feb 20 02:05 600144F007D38C0000005A8B58410002.lu
-rw-r--r--   1 root     root         478 Feb 20 02:05 600144F007D38C0000005A8B58410003.lu
-rw-r--r--   1 root     root         478 Feb 20 02:05 600144F007D38C0000005A8B58420004.lu
-rw-r--r--   1 root     root         478 Feb 20 02:05 600144F007D38C0000005A8B58420005.lu
-rw-r--r--   1 root     root         478 Feb 20 02:05 600144F007D38C0000005A8B58420006.lu
-rw-r--r--   1 root     root         478 Feb 20 02:05 600144F007D38C0000005A8B58420007.lu
-rw-r--r--   1 root     root         478 Feb 20 02:05 600144F007D38C0000005A8B58430008.lu
-rw-r--r--   1 root     root         478 Feb 20 02:05 600144F007D38C0000005A8B58430009.lu
-rw-r--r--   1 root     root         480 Feb 20 02:05 600144F007D38C0000005A8B5843000A.lu
-rw-r--r--   1 root     root          21 Feb 20 01:28 cinder-dev-tpg.tpg
-rw-r--r--   1 root     root         122 Feb 20 01:28 iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371.target

root@znstor6-n1:~# cat /dpool5/.stmfha/600144F007D38C0000005A8B58420004.lu
{"600144F007D38C0000005A8B58420004": {"VendorID": "SUN", "DataFile": "/dev/zvol/rdsk/dpool5/vols/vol-4", "WriteProtect": "Disabled", "ProviderName": "sbd", "MetaFile": "notset", "ViewEntryCount": 0, "WritebackCache": "Enabled", "Alias": "/dev/zvol/rdsk/dpool5/vols/vol-4", "OperationalStatus": "Online", "SerialNum": "notset", "AccessState": "Active", "BlockSize": 512, "WriteCacheModeSelect": "Enabled", "ProductID": "COMSTAR", "ManagementURL": "notset", "Size": 107374182400}}
```

### Add export
```bash
root@znstor6-n1:~# stmfha create-hg test-hg
root@znstor6-n1:~# stmfha add-hg-member -g test-hg iqn.1986-03.com.sun:01:e00000000000.5a607ec1
  
root@znstor6-n1:~# stmfha create-tg test-tg
root@znstor6-n1:~# stmfha add-tg-member -g test-tg iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371
 
root@znstor6-n1:~# for i in $(stmfadm list-lu |awk '{print $NF}'); do stmfha add-view -t test-tg -h test-hg $i; done

root@znstor6-n1:~# ls -l /dpool5/.stmfha
total 456
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B583E0001.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B583E0001_0.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58410002.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58410002_0.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58410003.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58410003_0.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58420004.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58420004_0.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58420005.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58420005_0.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58420006.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58420006_0.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58420007.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58420007_0.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58430008.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58430008_0.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58430009.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58430009_0.export
-rw-r--r--   1 root     root         442 Feb 20 02:09 600144F007D38C0000005A8B5843000A.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B5843000A_0.export
-rw-r--r--   1 root     root          21 Feb 20 02:09 cinder-dev-tpg.tpg
-rw-r--r--   1 root     root         122 Feb 20 02:09 iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371.target
-rw-r--r--   1 root     root          48 Feb 20 02:09 test-hg.hg
-rw-r--r--   1 root     root          63 Feb 20 02:10 test-tg.tg
 
root@znstor6-n1:~# stmfha create-hg test-hg2
root@znstor6-n1:~# stmfha add-hg-member -g test-hg2 iqn.1986-03.com.sun:01:e00000000000.5a607ec2
root@znstor6-n1:~# for i in $(stmfadm list-lu |awk '{print $NF}'); do stmfha add-view -t test-tg -h test-hg2 $i; done
 
root@znstor6-n1:~# ls -l /dpool5/.stmfha
total 665
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B583E0001.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B583E0001_0.export
-rw-r--r--   1 root     root          61 Feb 20 02:12 600144F007D38C0000005A8B583E0001_1.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58410002.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58410002_0.export
-rw-r--r--   1 root     root          61 Feb 20 02:12 600144F007D38C0000005A8B58410002_1.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58410003.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58410003_0.export
-rw-r--r--   1 root     root          61 Feb 20 02:12 600144F007D38C0000005A8B58410003_1.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58420004.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58420004_0.export
-rw-r--r--   1 root     root          61 Feb 20 02:12 600144F007D38C0000005A8B58420004_1.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58420005.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58420005_0.export
-rw-r--r--   1 root     root          61 Feb 20 02:12 600144F007D38C0000005A8B58420005_1.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58420006.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58420006_0.export
-rw-r--r--   1 root     root          61 Feb 20 02:12 600144F007D38C0000005A8B58420006_1.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58420007.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58420007_0.export
-rw-r--r--   1 root     root          61 Feb 20 02:12 600144F007D38C0000005A8B58420007_1.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58430008.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58430008_0.export
-rw-r--r--   1 root     root          61 Feb 20 02:12 600144F007D38C0000005A8B58430008_1.export
-rw-r--r--   1 root     root         440 Feb 20 02:09 600144F007D38C0000005A8B58430009.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B58430009_0.export
-rw-r--r--   1 root     root          61 Feb 20 02:12 600144F007D38C0000005A8B58430009_1.export
-rw-r--r--   1 root     root         442 Feb 20 02:09 600144F007D38C0000005A8B5843000A.lu
-rw-r--r--   1 root     root          60 Feb 20 02:11 600144F007D38C0000005A8B5843000A_0.export
-rw-r--r--   1 root     root          61 Feb 20 02:12 600144F007D38C0000005A8B5843000A_1.export
-rw-r--r--   1 root     root          21 Feb 20 02:09 cinder-dev-tpg.tpg
-rw-r--r--   1 root     root         122 Feb 20 02:09 iqn.1986-03.com.sun:02:8d92ead8-c8e8-4bad-b4aa-a49fc62a7371.target
-rw-r--r--   1 root     root          48 Feb 20 02:09 test-hg.hg
-rw-r--r--   1 root     root          48 Feb 20 02:12 test-hg2.hg
-rw-r--r--   1 root     root          63 Feb 20 02:10 test-tg.tg
```

### Try to failover
```bash
root@znstor6-n2:/opt/stmfha# time clrg switch -n  znstor6-n1 dpool5-rg

real    0m25.041s
user    0m0.025s
sys     0m0.011s

root@znstor6-n1:~# stmfadm list-lu
LU Name: 600144F007D38C0000005A8B583E0001
LU Name: 600144F007D38C0000005A8B58410002
LU Name: 600144F007D38C0000005A8B58410003
LU Name: 600144F007D38C0000005A8B58420004
LU Name: 600144F007D38C0000005A8B58420005
LU Name: 600144F007D38C0000005A8B58420006
LU Name: 600144F007D38C0000005A8B58420007
LU Name: 600144F007D38C0000005A8B58430008
LU Name: 600144F007D38C0000005A8B58430009
LU Name: 600144F007D38C0000005A8B5843000A

root@znstor6-n2:/opt/stmfha# time clrg switch -n  znstor6-n2 dpool5-rg

real    0m20.569s
user    0m0.035s
sys     0m0.012s
```